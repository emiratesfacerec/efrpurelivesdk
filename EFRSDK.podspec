Pod::Spec.new do |spec|
  spec.name          = 'EFRSDK'
  spec.version       = '4.1.11'
  spec.homepage      = 'https://bitbucket.org/emiratesfacerec/efrpurelivesdk'
  spec.authors       = { 'AICENTER' => 'info@aicenter.ae' }
  spec.summary       = 'PureLive SDK for face detection and liveness check'
  spec.source        = { :git => 'https://bitbucket.org/emiratesfacerec/efrpurelivesdk.git', :tag => '4.1.11', :submodules => true  }
  #spec.swift_version = '5.4'
  spec.swift_versions = ['5.4','5.5']
  #spec.default_subspec = 'Full'
  spec.vendored_frameworks = 'EFRSDK.xcframework','PureLiveSDK.xcframework'
  spec.ios.deployment_target  = '11.0'
  spec.static_framework = true
  #spec.resources = 'PureLiveSDKResources.bundle'


  spec.dependency 'TensorFlowLiteC', '2.7.0'
 # spec.static_framework = true
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
 # spec.dependency 'PureLiveSDK', '2.2.5'

end
